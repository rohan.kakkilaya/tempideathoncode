import React, {memo, useRef, useState, useCallback, useEffect} from 'react';
import {
  Animated,
  View,
  StyleSheet,
  Image,
  Text,
  Platform,
  TouchableOpacity,
} from 'react-native';
import {Props} from '../types/auth';
import images from '../../assets/images';
import MyViewHeader from '../../components/MyViewHeader';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

const SolarPairScreen: React.FC<Props> = ({navigation}) => {
  const fadeAnim = useRef(new Animated.Value(0)).current;
  const fadeAnim2 = useRef(new Animated.Value(0)).current;
  const fadeAnim3 = useRef(new Animated.Value(0)).current;
  const [isPairing, setIsPairing] = useState(false);
  const [pairText, setPairText] = useState('Pair');
  const [pairButtonColor, setPairButtonColor] = useState('white');
  const [pairTextColor, setPairTextColor] = useState('rgb(72, 108, 131)');

  const handlePairClick = useCallback(() => {
    setIsPairing(!isPairing);
    Animated.loop(
      Animated.sequence([
        Animated.timing(fadeAnim3, {
          toValue: 1,
          duration: 500,
          useNativeDriver: false,
        }),
        Animated.timing(fadeAnim3, {
          toValue: 0,
          duration: 500,
          useNativeDriver: false,
        }),
        Animated.timing(fadeAnim2, {
          toValue: 1,
          duration: 500,
          useNativeDriver: false,
        }),
        Animated.timing(fadeAnim2, {
          toValue: 0,
          duration: 500,
          useNativeDriver: false,
        }),
        Animated.timing(fadeAnim, {
          toValue: 1,
          duration: 500,
          useNativeDriver: false,
        }),
        Animated.timing(fadeAnim, {
          toValue: 0,
          duration: 500,
          useNativeDriver: false,
        }),
      ]),
      {iterations: 2},
    ).start();
  }, [fadeAnim, fadeAnim2, fadeAnim3, isPairing]);

  const handlePairSuccess = useCallback(() => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(fadeAnim3, {
          toValue: 1,
          duration: 500,
          useNativeDriver: false,
        }),
        Animated.timing(fadeAnim3, {
          toValue: 0,
          duration: 500,
          useNativeDriver: false,
        }),
      ]),
    ).start();
    Animated.loop(
      Animated.sequence([
        Animated.timing(fadeAnim2, {
          toValue: 1,
          duration: 500,
          useNativeDriver: false,
        }),
        Animated.timing(fadeAnim2, {
          toValue: 0,
          duration: 500,
          useNativeDriver: false,
        }),
      ]),
    ).start();
    Animated.loop(
      Animated.sequence([
        Animated.timing(fadeAnim, {
          toValue: 1,
          duration: 500,
          useNativeDriver: false,
        }),
        Animated.timing(fadeAnim, {
          toValue: 0,
          duration: 500,
          useNativeDriver: false,
        }),
      ]),
    ).start();
  }, [fadeAnim, fadeAnim2, fadeAnim3]);

  useEffect(() => {
    if (isPairing) {
      setPairText('Pairing...');
      setTimeout(() => {
        setPairText('Paired');
        setPairTextColor('white');
        setPairButtonColor('rgb(126,211,33)');
        handlePairSuccess();
      }, 9000);
      setPairTextColor('rgb(72, 108, 131)');
      setPairButtonColor('white');
    } else {
      setPairText('Pair');
    }
  }, [fadeAnim, fadeAnim2, fadeAnim3, handlePairSuccess, isPairing]);

  return (
    <View style={styles.base}>
      <View style={{paddingTop: hp('3%')}}>
        <MyViewHeader
          navigation={navigation}
          headerTitle={'Pairing'}
          addIconPress={() => {}}
          isIcon={true}
          hasAddIcon={false}
        />
      </View>
      <View style={styles.container}>
        <Text style={styles.nwcText}>NWC</Text>
        <Image source={images.cpuIcon} style={styles.cpuIcon} />
        <View style={{opacity: isPairing ? 1 : 0}}>
          <Animated.View style={[styles.circle, {opacity: fadeAnim}]} />
          <Animated.View style={[styles.circle, {opacity: fadeAnim2}]} />
          <Animated.View style={[styles.circle, {opacity: fadeAnim3}]} />
        </View>
        <Image source={images.wifiIcon} style={styles.wifiIcon} />
        <TouchableOpacity
          style={[styles.pairButton, {backgroundColor: pairButtonColor}]}
          onPress={handlePairClick}>
          <Text style={[styles.pairText, {color: pairTextColor}]}>
            {pairText}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  base: {
    flex: 1,
    width: '100%',
    height: '100%',
    objectFit: 'cover',
  },
  nwcText: {
    ...Platform.select({
      ios: {fontFamily: 'IBMPlexSans'},
      android: {fontFamily: 'IBMPlexSans-Regular'},
    }),
    fontSize: 16,
    fontWeight: 'bold',
    color: 'rgb(72, 108, 131)',
  },
  container: {
    flexDirection: 'column',
    width: '100%',
    height: '80%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cpuIcon: {
    height: 50,
    width: 50,
  },
  wifiIcon: {
    height: 50,
    width: 50,
  },
  circle: {
    backgroundColor: 'black',
    borderRadius: 50,
    height: 25,
    width: 25,
    marginVertical: 25,
  },
  pairButton: {
    marginTop: 30,
    backgroundColor: 'white',
    paddingVertical: 15,
    paddingHorizontal: 30,
    borderRadius: 8,
  },
  pairText: {
    ...Platform.select({
      ios: {fontFamily: 'IBMPlexSans'},
      android: {fontFamily: 'IBMPlexSans-Regular'},
    }),
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default memo(SolarPairScreen);
