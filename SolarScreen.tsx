import React, {memo} from 'react';
import {View, StyleSheet, ImageBackground} from 'react-native';
import {Props} from '../types/auth';
import images from '../../assets/images';
import SolarCard from './SolarCard';
import MyViewHeader from '../../components/MyViewHeader';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import BatteryHealthCard from './BatteryHealthCard';

const SolarScreen: React.FC<Props> = ({navigation}) => {
  return (
    <View style={styles.base}>
      <View style={{paddingTop: hp('3%')}}>
        <MyViewHeader
          navigation={navigation}
          headerTitle={'Solar with View'}
          addIconPress={() => {
            navigation.navigate('SolarPair');
          }}
          isIcon={true}
          hasAddIcon={true}
        />
      </View>
      <ImageBackground source={images.solarBackground} style={styles.base}>
        <View style={{marginTop: -30}}>
          <SolarCard
            descriptionText={'Solar cell output: 0.0 Watts'}
            icon={images.lightningIcon}
          />
        </View>
        <SolarCard
          descriptionText={'Battery Percentage: 80%'}
          icon={images.batteryIcon}
        />
        <BatteryHealthCard
          descriptionText={'PV Cell Degradation: 4.24%'}
          icon={images.batteryHealthIcon}
        />
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  base: {
    flex: 1,
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    justifyContent: 'center',
  },
});

export default memo(SolarScreen);
