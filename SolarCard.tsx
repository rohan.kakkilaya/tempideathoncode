import React, {FC, memo} from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';

interface SolarCardProps {
  descriptionText: string;
  icon: any;
}

const SolarCard: FC<SolarCardProps> = ({descriptionText, icon}) => {
  return (
    <View style={styles.card}>
      <View style={styles.iconContainer}>
        <Image source={icon} style={styles.icon} />
      </View>
      <View style={styles.descriptionContainer}>
        <Text style={styles.description}>{descriptionText}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#bfbfbf',
    marginVertical: 15,
    marginHorizontal: 15,
    backgroundColor: 'white',
    display: 'flex',
    padding: 15,
  },
  iconContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: 50,
    height: 50,
    tintColor: 'rgb(72, 108, 131)',
  },
  descriptionContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
  },
  description: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'rgb(72, 108, 131)',
  },
});

export default memo(SolarCard);
